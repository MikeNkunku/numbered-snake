# Numbered Snake

## Description
This single-file project aims at calculating the matrix of a snake with the matrix being provided by the user.<br />
One can also choose whether to have the resulting snake displayed with **right alignment** (or **left alignment** by default).

## Usage
|Variable location|Purpose|Mandatory|
|:---:|---|:---:|
|args[0]|Size of the matrix|Yes|
|args[1]|Use right alignment (`ar` for **align right**)|No|