import java.util.Arrays;

/**
 * Application which displays a numbered snake according to the size provided by the user.
 *
 * @author Mike.
 */
public class NumberedSnake {

	/** Argument ID which indicates to use right alignment. */
	private static final String USE_RIGHT_ALIGNMENT = "ar";


	/**
	 * @param args Arguments received from command line.
	 */
	public static void main(final String[] args) {
		int size = 1;
		try {
			size = getSize(args);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			return;
		}

		if (size == 1) {
			System.out.println(1);
			return;
		}

		Snake snake = new Snake(size);
		snake.start();

		boolean useRightAlignment = false;
		if (args.length > 1) {
			useRightAlignment = USE_RIGHT_ALIGNMENT.equals(args[1]);
		}
		prettyPrint(snake, useRightAlignment);
	}

	/**
	 * @param args The arguments provided by the user.
	 * @return The matrix size.
	 * @throws IllegalArgumentException When no argument is provided or an invalid size is received.
	 */
	private static int getSize(final String[] args) throws IllegalArgumentException {
		if (args.length < 1) {
			throw new IllegalArgumentException("RTFM.");
		}

		int size = Integer.parseInt(args[0]);
		if (size < 1) {
			throw new IllegalArgumentException("Yeah... I get it: Mrs or Mr SmartPants trying to destroy the house... WON'T HAPPEN!");
		}
		return size;
	}

	/**
	 * <p>
	 * Displays the matrix of the provided {@link Snake Snake} in a <b>pretty</b> way.
	 * </p>
	 * For a size of <b>3</b>:
	 * <pre>
	 *     1 2 3
	 *     8 9 4
	 *     7 6 5
	 * </pre>
	 * @param snake The {@link Snake Snake} instance to display the matrix from.
	 * @param useRightAlignment Whether to use right alignment.
	 */
	private static void prettyPrint(final Snake snake, final boolean useRightAlignment) {
		final StringBuilder sb = new StringBuilder();
		int finalElementLength = String.valueOf(snake.getFinalElement()).length();
		Arrays.stream(snake.getMatrix()).forEach(row -> {
			Arrays.stream(row).forEach(cell -> {
				String strValue = String.valueOf(cell);
				if (useRightAlignment) {
					appendCellWithRightAlignment(sb, strValue, finalElementLength);
				} else {
					appendCellWithLeftAlignment(sb, strValue, finalElementLength);
				}
			});
			sb.append("\n\n");
		});
		System.out.print(sb.toString());
	}

	/**
	 * @param stringBuilder The string builder to concatenate the formatted cell's value to.
	 * @param cellValue The cell value.
	 * @param maximumCellLength The maximum length a cell can have.
	 */
	private static void appendCellWithLeftAlignment(final StringBuilder stringBuilder, final String cellValue, final int maximumCellLength) {
		stringBuilder.append(cellValue);
		for (int i = 0; i < maximumCellLength - cellValue.length() + 1; i++) {
			stringBuilder.append(' ');
		}
	}

	/**
	 * @param stringBuilder The string builder to concatenate the formatted cell's value to.
	 * @param cellValue The cell value.
	 * @param maximumCellLength The maximum length a cell can have.
	 */
	private static void appendCellWithRightAlignment(final StringBuilder stringBuilder, final String cellValue, final int maximumCellLength) {
		for (int i = 0; i < maximumCellLength - cellValue.length() + 1; i++) {
			stringBuilder.append(' ');
		}
		stringBuilder.append(cellValue);
	}


	/**
	 * POJO for the main method.
	 */
	private static final class Snake {

		/** The initial value the matrix is filled with. */
		private static final int INITIAL_MATRIX_ELEMENT_VALUE = -1;
		/** The initial value to set for the start point of the snake. */
		private static final int INITIAL_ELEMENT = 1;

		/** The matrix the instance has to run through. */
		private final int[][] matrix;
		/** The instance's current direction. */
		private Direction currentDirection;
		/** The size of the matrix. */
		private final int size;
		/** The value of the current element. */
		private int currentElement;
		/** The last value to be put in the matrix. */
		private final int finalElement;
		/** The index of the current row in the matrix. */
		private int currentRow;
		/** The index of the current column in the matrix. */
		private int currentColumn;

		/**
		 * Constructor.
		 *
		 * @param size The matrix size.
		 */
		public Snake(final int size) {
			this.size = size;
			this.currentElement = INITIAL_ELEMENT;
			this.finalElement = (int) Math.pow(size, 2);
			this.currentDirection = Direction.RIGHT;
			this.matrix = new int[this.size][this.size];
			this.currentRow = 0;
			this.currentColumn = 0;
			Arrays.stream(this.matrix).forEach(row -> Arrays.fill(row, INITIAL_MATRIX_ELEMENT_VALUE));
			this.matrix[0][0] = INITIAL_ELEMENT;
		}

		/** @return The matrix. */
		public int[][] getMatrix() {
			return this.matrix;
		}

		/** @return The final element. */
		public int getFinalElement() { return this.finalElement; }

		/**
		 * Starts moving the snake and changes direction.
		 *
		 * It stops when the snake has run through all its matrix.
		 */
		public void start() {
			while (!this.isMatrixFilled()) {
				this.moveInCurrentDirection();
				this.setNextDirection();
			}
		}

		/**
		 * Moves the snake in the current direction until it is not possible.
		 */
		private void moveInCurrentDirection() {
			while(this.fillNextCellInCurrentDirection()) {
				this.advance();
			}
		}

		/**
		 * Advances the snake from one cell in the matrix according to the current direction.
		 */
		private void advance() {
			switch (this.currentDirection) {
				case RIGHT: this.currentColumn++; break;
				case DOWN: this.currentRow++; break;
				case LEFT: this.currentColumn--; break;
				default: this.currentRow--;
			}
			this.matrix[this.currentRow][this.currentColumn] = ++this.currentElement;
		}

		/**
		 * @return Whether the next, in the current direction, can/must be filled.
		 */
		private boolean fillNextCellInCurrentDirection() {
			int nextCC = this.currentColumn;
			int nextCR = this.currentRow;;
			switch (this.currentDirection) {
				case RIGHT: nextCC++; break;
				case DOWN: nextCR++; break;
				case LEFT: nextCC--; break;
				default: nextCR--;
			}
			boolean isOffLimits = nextCC >= this.size || nextCR >= this.size || nextCC < 0 || nextCR < 0;
			if (isOffLimits) {
				return false;
			}
			return this.matrix[nextCR][nextCC] == INITIAL_MATRIX_ELEMENT_VALUE;
		}

		/**
		 * Updates current direction by setting the next one.
		 */
		private void setNextDirection() {
			Direction newDirection;
			switch (this.currentDirection) {
				case RIGHT: newDirection = Direction.DOWN; break;
				case DOWN: newDirection = Direction.LEFT; break;
				case LEFT: newDirection = Direction.UP; break;
				default: newDirection = Direction.RIGHT;
			}
			this.currentDirection = newDirection;
		}

		/**
		 * @return Whether the matrix is filled.
		 */
		private boolean isMatrixFilled() {
			return Arrays.stream(this.matrix).noneMatch(row -> Arrays.stream(row).anyMatch(value -> value == INITIAL_MATRIX_ELEMENT_VALUE));
		}
	}


	/**
	 * Enumeration class representing the directions that the {@link Snake Snake} can have.
	 */
	private enum Direction {
		RIGHT, DOWN, LEFT, UP;
	}
}
